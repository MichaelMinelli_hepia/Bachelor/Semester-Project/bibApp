import Foundation

/**
 *	@author Michaël Minelli, 16.01.2016
 *
 *	Class which contains a static method for do a HTTP request
 */
open class HTTPRequest {
    fileprivate static let defaultTimeout: Double = 5.0
    fileprivate static let charset: String = "UTF-8"
    
    /**
     Do a request (without paramters)
     
     - parameter endPointAddress:	Server address
     - parameter requestMethod:		Http Method
     - parameter timeout:			(Optional) Timeout of the request
     
     - returns: The response of the request in a HTTPRequestResult object
     */
    open static func submit(_ endPointAddress: String, requestMethod: HTTPRequestMethod, withTimeout timeout: Double = defaultTimeout) -> HTTPRequestResult {
        
        return submit(endPointAddress, requestMethod: requestMethod, requestParameters: [String: String](), withTimeout: timeout)
    }
    
    /**
     Do a request
     
     - parameter endPointAddress:		Server address
     - parameter requestMethod:			Http Method
     - parameter requestParameters:		Parameters of the request. Format in an array [key: value]. If the method is POST, paramters are in the body otherwise in the url
     - parameter timeout:				(Optional) Timeout of the request
     
     - returns: The response of the request in a HTTPRequestResult object
     */
    open static func submit(_ endPointAddress: String, requestMethod: HTTPRequestMethod, requestParameters: [String: String], withTimeout timeout: Double = defaultTimeout) -> HTTPRequestResult {
        
        var result: HTTPRequestResult = HTTPRequestResult()
        
        var parameters: String = ""
        
        //Construct the string of parameters
        for (key, value) in requestParameters {
            parameters += key + "=" + value.urlEncode() + "&"
        }
        
        //Create the URL object
        var url:URL? = nil
        if requestMethod == HTTPRequestMethod.POST {
            url = URL(string: endPointAddress)
        } else {
            //If the method is not POST, add the parameters string to the address
            url = URL(string: endPointAddress + "?" + parameters)
        }
        
        //Create the request object
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = requestMethod.rawValue
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData
        request.timeoutInterval = timeout
        
        //If the method is POST, add the parameters in the body
        if requestMethod == HTTPRequestMethod.POST {
            let data = parameters.data(using: String.Encoding.utf8, allowLossyConversion: true)!
            
            request.setValue("application/x-www-form-urlencoded;charset=\(charset)", forHTTPHeaderField: "Content-Type")
            request.setValue(charset, forHTTPHeaderField: "Accept-Charset")
            request.setValue("\(data.count)", forHTTPHeaderField: "Content-Length")
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.httpBody = data
        }
        
        //Semaphore used like a mutex, for transform the assynchronous request in a synchronous request
        let recuperationFinished:DispatchSemaphore = DispatchSemaphore(value: 0)
        
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let task = session.dataTask(with: request as URLRequest) {
            (data, response, error) -> Void in
            
            //Detect possible error
            if error == nil {
                if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != HTTPStatusCode.OK {
                    result.responseCode = httpStatus.statusCode
                } else if let data = data {
                    result.affectResponse(data)
                }
            } else {
                result.responseCode = HTTPStatusCode.ERROR
            }
            
            recuperationFinished.signal()
        }
        
        //Start the background thread
        task.resume()
        
        //Wait for the end of the data recuperation
        recuperationFinished.wait(timeout: DispatchTime.distantFuture)
        
        return result
    }
}
