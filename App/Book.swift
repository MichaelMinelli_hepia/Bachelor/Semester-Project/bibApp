import Foundation
import Kanna

/**
 *	@author Michaël Minelli, 16.01.2016
 *
 *	Datas of a book
 */
open class Book: NSObject, NSCoding {
    fileprivate static let NEBIS_AVAILABILITY_PAGE = "http://recherche.nebis.ch/primo_library/libweb/action/display.do?ct=display&fn=search&doc=%@&indx=1&recIds=%@&recIdxs=0&elementId=0&renderMode=poppedOut&displayMode=full&dscnt=0&dstmp=1455454195038&vid=default&tabs=locationsTab&gathStatTab=true"
    
    /**
     *	@author Michaël Minelli, 13.02.2016
     *
     *	Enumeration of state that can be a book
     */
    public enum AvailabilityState: String {
        case Available = "Disponible", OutOfLibrary = "Indisponible", NotDefined = "Non défini"
    }
    
    /**
     *	@author Michaël Minelli, 13.02.2016
     *
     *	Enumeration of library
     */
    public enum Library: String {
        case hepia = "", lullier = " (site Lullier)"
    }
    
    open var id: String
    open var title: String
    open var author: String
    open var quote: String
    open var image: String
    open var resume: String
    open var language: String
    open var applicant: String
    open var year: String
    open var desc: String
    open var editor: String
    open var section: Int
    
    init(id: String, title: String, author: String, quote: String, image: String, resume: String, language: String, applicant: String, year: String, desc: String, editor: String, section: Int) {
        self.id = id
        self.title = title
        self.author = author
        self.quote = quote
        self.image = image
        self.resume = resume
        self.language = language
        self.applicant = applicant
        self.year = year
        self.desc = desc
        self.editor = editor
        self.section = section
    }
    
    public required init(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObject(forKey: "id") as! String
        self.title = aDecoder.decodeObject(forKey: "title") as! String
        self.author = aDecoder.decodeObject(forKey: "author") as! String
        self.quote = aDecoder.decodeObject(forKey: "quote") as! String
        self.image = aDecoder.decodeObject(forKey: "image") as! String
        self.resume = aDecoder.decodeObject(forKey: "resume") as! String
        self.language = aDecoder.decodeObject(forKey: "language") as! String
        self.applicant = aDecoder.decodeObject(forKey: "applicant") as! String
        self.year = aDecoder.decodeObject(forKey: "year") as! String
        self.desc = aDecoder.decodeObject(forKey: "desc") as! String
        self.editor = aDecoder.decodeObject(forKey: "editor") as! String
        self.section = aDecoder.decodeInteger(forKey: "section")
    }
    
    open func encode(with aCoder: NSCoder) {
        aCoder.encode(self.id, forKey: "id")
        aCoder.encode(self.title, forKey: "title")
        aCoder.encode(self.author, forKey: "author")
        aCoder.encode(self.quote, forKey: "quote")
        aCoder.encode(self.image, forKey: "image")
        aCoder.encode(self.resume, forKey: "resume")
        aCoder.encode(self.language, forKey: "language")
        aCoder.encode(self.applicant, forKey: "applicant")
        aCoder.encode(self.year, forKey: "year")
        aCoder.encode(self.desc, forKey: "desc")
        aCoder.encode(self.editor, forKey: "editor")
        aCoder.encode(self.section, forKey: "section")
    }
    
    /**
     Function that search the availability state of the book and return the response to a selector
     
     - parameter target: Object with the selector function
     - parameter action: Selector
     */
    open func isAvailable(target: AnyObject, action: Selector) {
        
        //Start loading in another thread
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.high).async(execute: { () in
            
            var state = AvailabilityState.NotDefined
            var site = Library.hepia
            
            //Do the request
            let httpRequestResult: HTTPRequestResult = HTTPRequest.submit(String(format: Book.NEBIS_AVAILABILITY_PAGE, self.id, self.id), requestMethod: .GET)
            
            //Verifiy if the request have work and extract data
            if httpRequestResult.responseCode == HTTPStatusCode.OK {
                
                //Start the html parsing
                if let doc = Kanna.HTML(html: httpRequestResult.response, encoding: String.Encoding.utf8) {
                    //Search each node corresponding to each library
                    for bib in doc.xpath("//div[@class='EXLLocationList']") {
                        if let bibHTML = bib.toHTML {
                            //Parse the current library
                            if let bibKanna = Kanna.HTML(html: bibHTML, encoding: String.Encoding.utf8) {
                                //Test the current library if it's hepia ("E41") or lullier ("E67")
                                if let bibRef = bibKanna.xpath("//span[@class='EXLLocationsTitleContainer']")[0].text, bibRef.range(of: "E41") != nil || bibRef.range(of: "E67") != nil {
                                    site = bibRef.range(of: "E67") != nil ? Library.lullier : Library.hepia
                                    
                                    //Le library have an error in the search into a em element so we use another method to extract the availability of the book (parsing string)
                                    if let bibHTMLrange = bibHTML.range(of: "Available") {
                                        let index = bibHTML.index(after: bibHTMLrange.lowerBound)
                                        
                                        if bibHTML.substring(from: index).range(of: "Available") != nil {
                                            state = AvailabilityState.Available
                                        } else if bibHTML.substring(from: index).range(of: "Out of library") != nil {
                                            state = AvailabilityState.OutOfLibrary
                                        }
                                        
                                        break
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            DispatchQueue.main.async(execute: { () in
                //Call the selector with the response
                target.perform(action, with: state.rawValue, with: site.rawValue)
            });
        })
    }
}
