import UIKit

class SectionTabBarController: UITabBarController, ControllerWithSettingsButton {
    
    var allSections: Bool = false
    var section:Section = Section(id: -1, name: "", acronym: "", code: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Global_IOS.setGlobalAppearance(self, withSettingsButton: true, withTabBarController: self)
        self.title = "Nouveautés"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        Global_IOS.runBeaconDetection(self)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        Global_IOS.ibeaconDetection?.stop()
    }
    
    func settingsButtonClick(_ sender: AnyObject) {
        Global_IOS.openSettingsView(self)
    }
}
