/**
 *	@author Michaël Minelli, 04.02.2016
 *
 *  Protocol which discribe a book cell. So we can change the style of cell (user preference) without to play with if
 */
protocol BookCell {
    func prepareCell(_ book: Book)
}
