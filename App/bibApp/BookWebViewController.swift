import UIKit

class BookWebViewController: UIViewController {
    
    @IBOutlet weak var toolBar: UIToolbar!
    @IBOutlet weak var webView: UIWebView!
    
    var url:URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Prepare the tool bar
        let safari = UIBarButtonItem(title: "Ouvrir dans Safari", style: .done, target: self, action: #selector(BookWebViewController.safariButtonClick(_:)))
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let close = UIBarButtonItem(title: "Retour au livre", style: .done, target: self, action: #selector(BookWebViewController.closeButtonClick(_:)))
        let space2 = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        
        toolBar.setItems([safari, space, close, space2], animated: true)
        
        if let url = self.url {
            webView.loadRequest(URLRequest(url: url))
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func closeButtonClick(_ sender: AnyObject) {
        //Close web view
        self.dismiss(animated: true, completion: nil)
    }
    
    func safariButtonClick(_ sender: AnyObject) {
        //Open Safari
        if let url = self.url {
            UIApplication.shared.openURL(url)
        }
    }
}

