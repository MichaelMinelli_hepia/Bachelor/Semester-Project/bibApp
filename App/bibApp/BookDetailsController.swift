import UIKit
import SafariServices
import Kanna

class BookDetailsController: UIViewController, SFSafariViewControllerDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var btNebis: UIButton!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAuthors: UILabel!
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var lblLanguage: UILabel!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var lblResume: UILabel!
    
    var book: Book?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Global_IOS.setGlobalAppearance(self, withSettingsButton: false)
        self.title = "Détails de l'ouvrage"
        
        btNebis.layer.cornerRadius = 5
        
        //Display the book informations
        if let book = self.book {
            lblTitle.text = book.title
            lblAuthors.text = book.author
            lblYear.text = book.year
            lblLanguage.text = book.language
            //lblQuote.text = book.quote == "" ? "--" : book.quote
            lblResume.text = book.desc + "\n" + book.resume
            
            book.isAvailable(target: self, action: #selector(BookDetailsController.updateBookState(_:fromSite:)))
        }
    }
    
    func updateBookState(_ availabilityState: String, fromSite site: String) {
        lblState.text = availabilityState + site
        
        let availabilityState = Book.AvailabilityState(rawValue: availabilityState)!
        
        switch availabilityState {
        case .Available:
            lblState.textColor = Colors.darkGreen
        case .OutOfLibrary:
            lblState.textColor = UIColor.red
        default:
            lblState.textColor = UIColor.darkGray
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        Global_IOS.runBeaconDetection(self)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        Global_IOS.ibeaconDetection?.stop()
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        
        if let book = self.book {
            if let url = URL(string: String(format: BibAppAPI.NEBIS_BOOK_PAGE, book.id)) {
                if #available(iOS 9, *) {
                    let svc = SFSafariViewController(url: url)
                    svc.delegate = self
                    svc.title = "Détails du livre sur NEBIS"
                    self.present(svc, animated: true, completion: nil)
                } else {
                    return true
                }
            }
        }
        
        return false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let webView = segue.destination as? BookWebViewController
        {
            webView.url = URL(string: String(format: BibAppAPI.NEBIS_BOOK_PAGE, book!.id))
        }
    }
}

