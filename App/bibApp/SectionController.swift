import UIKit

class SectionController: UIViewController, UITableViewDataSource, UITableViewDelegate, ControllerWithSettingsButton {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Global_IOS.setGlobalAppearance(self, withSettingsButton: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        Global_IOS.runBeaconDetection(self)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        Global_IOS.ibeaconDetection?.stop()
    }
    
    func settingsButtonClick(_ sender: AnyObject) {
        Global_IOS.openSettingsView(self)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Global_IOS.bibApp.getSections().count + 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //If it's the first row the size is different because it's the row "Toutes les nouveautés"
        return indexPath.row == 0 ? 50 : 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell
        
        //Dequeue the good cell
        if indexPath.row == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "AllNews", for: indexPath)
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "SectionCell", for: indexPath)
            
            //Prepare the cell with section informations
            (cell as! SectionCell).prepareCell(Global_IOS.bibApp.getSections()[indexPath.row - 1])
        }
        
        //Alternate the background color
        cell.backgroundColor = indexPath.row % 2 == 1 ? Colors.lineBackground2 : Colors.lineBackground1
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //Affect informations to the next view controller
        if let row = self.tableView.indexPathForSelectedRow?.row {
            (segue.destination as! SectionTabBarController).allSections = row == 0
            
            if row > 0 {
                (segue.destination as! SectionTabBarController).section = Global_IOS.bibApp.getSections()[row - 1]
            }
        }
    }
}

