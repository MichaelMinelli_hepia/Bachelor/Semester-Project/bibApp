import Foundation

/**
 *	@author Michaël Minelli, 16.01.2016
 *
 *	Enumeration of HTTP method available
 *  Use .rawValue for obtain the String equivalent
 */
public enum HTTPRequestMethod: String {
    case GET, POST, PUT, DELETE
}