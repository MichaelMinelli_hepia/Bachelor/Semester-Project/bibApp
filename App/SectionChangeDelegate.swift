import Foundation

/**
 *	@author Michaël Minelli, 04.02.2016
 *
 *  Delegate for the dectecion of a new region
 */
public protocol SectionChangeDelegate {
    func sectionChange(newSection: Int)
}
