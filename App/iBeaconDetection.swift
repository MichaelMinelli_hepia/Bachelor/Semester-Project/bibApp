import Foundation
import CoreLocation

/**
 Enumaration that describe all error that can be throw by the iBeacon Detection
 
 - LocationAccessDenied:    Access to the location denied by the system (user don't agree with the location)
 - LocationNotAvailable:    Access to the location not available (disabled in the system)
 - LocationErrorNotDefined: Access to the location don't can be done beacaus of a unknown error
 */
public enum iBeaconDetectionError: Error {
    case locationAccessDenied
    case locationNotAvailable
    case locationErrorNotDefined
}

/**
 *	@author Michaël Minelli, 04.02.2016
 *
 *	Detection of iBeacon beacons
 */
open class iBeaconDetection: NSObject, BeaconDetection, CLLocationManagerDelegate {
    open var delegate: BeaconDetectionDelegate //Delegate of the detection of a new beacon
    open var region: CLBeaconRegion //Beacon region
    
    var locationManager: CLLocationManager!
    
    //This constant indicate the number of time before a new beacon is accepted like that. If a new beacon is detected x times in a row it's accepted like the new most near beacon
    //This is necessary beacause there is some imprecision in the calculation of the distance. So a one time detection is not a garantee.
    let LIMIT_BEFORE_DETECTION: Int = 4
    var actualBeacon: CLBeacon = CLBeacon()
    var beaconDetectionCounter:Int = 0
    
    init(region: CLBeaconRegion, delegate: BeaconDetectionDelegate) {
        self.region = region
        self.delegate = delegate
    }
    
    /**
     Run the detection
     
     - throws: Error from iBeaconDetectionError enumeration
     */
    func run() throws {
        //Creation of the location manager and indicate to this object that the delegate is the current object
        locationManager = CLLocationManager()
        locationManager.delegate = self
        
        //Indicate to the system that he have to always search beacons even when the apps is closed
        region.notifyOnEntry = true
        region.notifyOnExit = true
        
        //Verify if we can start the location or ask for authorization
        if CLLocationManager.locationServicesEnabled() {
            if CLLocationManager.authorizationStatus() ==  CLAuthorizationStatus.notDetermined {
                locationManager.requestAlwaysAuthorization() //Ask for location
            } else if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.denied {
                throw iBeaconDetectionError.locationAccessDenied
            } else if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.restricted {
                throw iBeaconDetectionError.locationNotAvailable
            }
            
            //Start the "location" (iBeacon region detection)
            locationManager.startMonitoring(for: self.region)
        } else {
            throw iBeaconDetectionError.locationErrorNotDefined
        }
    }
    
    /**
     Stop the detection
     */
    func stop() {
        locationManager.stopMonitoring(for: self.region)
        locationManager.stopRangingBeacons(in: self.region)
    }
    
    open func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        //If we have authorization, start the iBeacon region detection
        if status != CLAuthorizationStatus.notDetermined && status != CLAuthorizationStatus.denied {
            locationManager.startMonitoring(for: self.region)
        }
    }
    
    open func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        //Call the delegate function for the enter region event
        self.delegate.beaconDetection(enterRegion: region)
    }
    
    open func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        
    }
    
    open func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
        //Ask for update for the region state
        locationManager.requestState(for: region)
    }
    
    open func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion) {
        //If we are in a beacon proximity, start the detection of beacon continuously
        if state == CLRegionState.inside || state == CLRegionState.unknown {
            locationManager.startRangingBeacons(in: self.region)
        } else {
            locationManager.stopRangingBeacons(in: self.region)
        }
    }
    
    open func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        var mostNear: CLBeacon? = nil
        var mostNearDIST: Double = 100000
        
        //Extract all beacon that we know the distance
        let knownBeacons = beacons.filter{ $0.proximity != CLProximity.unknown }

        //Determine the most near beacon
        for beacon in knownBeacons {
            if beacon.accuracy < mostNearDIST {
                mostNearDIST = beacon.accuracy
                mostNear = beacon
            }
        }
        
        //If a most near beacon was found
        if let mostNear = mostNear {
            //Verify if it is a new beacon not the current most near
            if mostNear.major != self.actualBeacon.major || mostNear.minor != self.actualBeacon.minor {
                //Increment the beacon detection counter
                self.beaconDetectionCounter += 1
                
                //If the beacon have been detected x times, update the most near beacon and execute the delegate method
                if self.beaconDetectionCounter >= self.LIMIT_BEFORE_DETECTION {
                    self.actualBeacon = mostNear
                    self.beaconDetectionCounter = 0
                    self.delegate.beaconDetection(mainBeaconChange: iBeacon(section: nil, major: self.actualBeacon.major.intValue, minor: self.actualBeacon.minor.intValue))
                }
            } else {
                self.beaconDetectionCounter = 0
            }
        }
    }
    
    open func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
    }
    
    open func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        
    }
    
    open func locationManager(_ manager: CLLocationManager,rangingBeaconsDidFailFor region: CLBeaconRegion,withError error: Error){
        
    }
}
