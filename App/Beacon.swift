import Foundation

/**
 *	@author Michaël Minelli, 04.02.2016
 *
 *  Protocal which describe a Beacon.
 *  A beacon is compose by his ID data (example : major, minor for iBeacon) that change in function of the beacon type and a section.
 *  This protocol is compose by the section ID.
 */
public protocol Beacon {
    var section: Int? { get set }
}