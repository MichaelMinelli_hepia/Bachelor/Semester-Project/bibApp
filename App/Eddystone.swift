import Foundation

/**
 *	@author Michaël Minelli, 16.01.2016
 *
 *	Datas of an eddystone beacon
 */
open class Eddystone: NSObject, Beacon, NSCoding {
    open var section: Int?
    
    open var instance: String
    
    init(section: Int?, instance: String) {
        self.section = section
        self.instance = instance
    }
    
    public required init(coder aDecoder: NSCoder) {
        self.section = aDecoder.decodeInteger(forKey: "section")
        self.instance = aDecoder.decodeObject(forKey: "instance") as! String
    }
    
    open func encode(with aCoder: NSCoder) {
        aCoder.encode(self.section!, forKey: "section")
        aCoder.encode(self.instance, forKey: "instance")
    }
}
