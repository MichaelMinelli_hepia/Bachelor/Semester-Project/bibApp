import Foundation
import UIKit

/**
 *	@author Michaël Minelli, 08.02.2016
 *
 *	Class which manage the changement of section
 */
class SectionChangeAction: SectionChangeDelegate {
    var viewController: UIViewController
    var alertView = SCLAlertView()
    
    //the new section
    fileprivate var section: Section = Section(id: -1, name: "", acronym: "", code: "")
    
    init(viewController: UIViewController) {
        //Current view controller
        self.viewController = viewController
    }
    
    /**
     Change section detection event (from protocol "SectionChangeDelegate")
     
     - parameter newSection: ID of the new section
     */
    func sectionChange(newSection: Int) {
        //Get section infos
        self.section = Global_IOS.bibApp.getSection(forID: newSection)!
        
        /* Other popup style
        let alertView = JSSAlertView().show(self.viewController, title: "Changement de zone", text: "Nous avons détecté que vous avez changé de zone de la bibliothèque. Voulez-vous ouvrir les nouveautés de la zone \"\(self.section.name)\" ?", buttonText: "Oui", cancelButtonText: "Non merci", color: Colors.primaryColor, iconImage: self.section.image)
        alertView.addAction(changeSection)
        alertView.setTextTheme(.Light)
        */
        
        //Alert view for ask the user if he want to change the current section
        
        //Hide and recreate AlertView for avoid a bug in the framework that add more than two buttons
        alertView.hideView()
        alertView = SCLAlertView()
        
        alertView.addButton("Oui") { self.changeSection() }
        
        alertView.showCustom("Changement de zone", subTitle: "Nous avons détecté que vous avez changé de zone de la bibliothèque. Voulez-vous ouvrir les nouveautés de la zone \"\(section.name)\" ?", duration: nil, completeText: "Non merci", iconImage: section.image, viewColor: Colors.primaryColor, colorTextButton: Colors.secondaryColor)
    }
    
    /**
     Function call by the alert view if the user agree to change section
     */
    fileprivate func changeSection() {
        //Prepare the new view controller
        let vc : SectionTabBarController = viewController.storyboard!.instantiateViewController(withIdentifier: "sectionTabBar") as! SectionTabBarController
        vc.allSections = false
        vc.section = self.section
        
        let navigationController = self.viewController.navigationController!
        navigationController.popToRootViewController(animated: false) //Go back to the first view controller (section list)
        navigationController.pushViewController(vc, animated: true) //Go to the section detail
    }
}
