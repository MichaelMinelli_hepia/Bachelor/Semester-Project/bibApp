import Foundation

extension Array {
    /**
     Find one (and only one) object corresponding of a predicate
     
     - parameter predicate:	Predicate which take an Element in parameter and return a boolean which indicate if the object is the search object
     
     - returns: The search object if it's found, nil otherwise
     */
    func find(_ predicate: (Element) -> Bool) -> Element? {
        for (_, element) in enumerated() {
            if predicate(element) {
                return element
            }
        }
        
        return nil
    }
}
