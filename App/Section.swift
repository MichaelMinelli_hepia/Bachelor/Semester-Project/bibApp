import Foundation
import UIKit

/**
 *	@author Michaël Minelli, 16.01.2016
 *
 *	Datas of a section
 */
open class Section: NSObject, NSCoding {
    open var id: Int
    open var name: String
    open var acronym: String
    open var code: String
    
    open var image: UIImage
    
    public init(id: Int, name: String, acronym: String, code: String) {
        self.id = id
        self.name = name
        self.acronym = acronym
        self.code = code
        
        let img = UIImage(named: self.acronym)
        self.image = (img != nil ? img : UIImage(named: "other"))!
    }
    
    public required init(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeInteger(forKey: "id")
        self.name = aDecoder.decodeObject(forKey: "name") as! String
        self.acronym = aDecoder.decodeObject(forKey: "acronym") as! String
        self.code = aDecoder.decodeObject(forKey: "code") as! String
        
        let img = UIImage(named: self.acronym)
        self.image = (img != nil ? img : UIImage(named: "other"))!
    }
    
    open func encode(with aCoder: NSCoder) {
        aCoder.encode(self.id, forKey: "id")
        aCoder.encode(self.name, forKey: "name")
        aCoder.encode(self.acronym, forKey: "acronym")
        aCoder.encode(self.code, forKey: "code")
    }
}
