import Foundation
import UIKit

/**
 *	@author Michaël Minelli, 16.01.2016
 *
 *	Interface for communication between backend REST server and application
 */
open class BibAppAPI {
    //Routes for communicate with the REST server
    fileprivate static let BIBAPP_API = "http://api.bibapp.infolibre.ch:7070/"
    //private static let BIBAPP_API = "http://bibApp.minelli.me/"
    fileprivate static let GET_SECTIONS = BibAppAPI.BIBAPP_API + "section"
    fileprivate static let GET_BEACONS_AND_BOOKS = BibAppAPI.BIBAPP_API + "allBeacons"
    
    open static let NEBIS_BOOK_PAGE = "http://recherche.nebis.ch/primo_library/libweb/action/display.do?ct=display&fn=search&doc=%@&indx=1&elementId=0&renderMode=poppedOut&displayMode=full&dscnt=0&dstmp=1457013576363&vid=NEBIS&tabs=locationsTab&gathStatTab=true&prefLang=fr_FR"
    
    //Default priority of the thread which load datas
    fileprivate static let DEFAULT_DATA_LOADING_PRIORITY = DispatchQueue.GlobalQueuePriority.high
    
    //Arrays of datas
    fileprivate var sections: [Section] = []
    fileprivate var books: [Book] = []
    fileprivate var iBeacons: [iBeacon] = []
    fileprivate var nearables: [Nearables] = [] //Not used
    fileprivate var eddystones: [Eddystone] = [] //Not used
    
    //Variables for manage the loading of datas
    fileprivate var sectionsIsLoaded: Bool = false
    fileprivate var sectionsIsLoading: Bool = false
    fileprivate let sectionsWaitOnLoad: Event = Event(flagValue: true)
    
    fileprivate var beaconsIsLoaded: Bool = false
    fileprivate var beaconsIsLoading: Bool = false
    fileprivate let beaconsWaitOnLoad: Event = Event(flagValue: true)
    
    fileprivate var loadFromLocalStorage: Bool
    
    /**
     Constructor of the class with the choice of load or not datas at the object creation
     
     - parameter loadAtCreation:	true for start immeditly the load of datas
     - parameter local:	true for load from the local storage if possible and if datas aren't expired
     */
    init(loadAtCreation: Bool, fromLocalStorage local: Bool) {
        self.loadFromLocalStorage = local
        
        if loadAtCreation {
            initData()
        }
    }
    
    /**
     Load all sections from the server
     
     - parameter wait:	This parameter indicate if we have to wait until the datas are load
     */
    open func loadSections(waitOnLoad wait: Bool) {
        //Verify if sections are not already loaded
        if !sectionsIsLoaded {
            //Verify if sections are not in loading
            if !sectionsIsLoading {
                //Set the flag of the Event to false for others attempt which want to load with wait
                sectionsWaitOnLoad.clear()
                
                let storage = Storage()
                var fromServer: Bool = true
                
                //If we need to load from local storage and data on the local storage are not expired
                if self.loadFromLocalStorage {
                    if let sections = storage.getSections(), !sections.isEmpty {
                        //If the load successful affect data loaded otherwise load from internet
                        self.sections = sections
                        
                        //If datas need to be updated try it
                        if !storage.isNeedToUpdateData() {
                            fromServer = false
                            sectionsIsLoaded = true
                            sectionsIsLoading = false
                            
                            //free all threads which wait to the load
                            self.sectionsWaitOnLoad.set()
                        }
                    }
                }
                
                if fromServer {
                    //Start loading in another thread
                    DispatchQueue.global(priority: BibAppAPI.DEFAULT_DATA_LOADING_PRIORITY).async(execute: { () in
                        //Define if the loading have work
                        var loaded: Bool = false
                        
                        //Indicate that the data are in loading
                        self.sectionsIsLoading = true
                        
                        //Do the request
                        let httpRequestResult: HTTPRequestResult = HTTPRequest.submit(BibAppAPI.GET_SECTIONS, requestMethod: .GET)
                        
                        //Verifiy if the request have work and extract data
                        if httpRequestResult.responseCode == HTTPStatusCode.OK {
                            if let json = httpRequestResult.jsonObject as? NSDictionary {
                                if let data = json["data"] as? NSDictionary {
                                    if let sections = data["sections"] as? [[String: Any]] {
                                        
                                        self.sections = []
                                        //Add each section found in the section data array
                                        for section in sections {
                                            self.sections.append(Section(id: section["id"] as! Int, name: section["name"] as! String, acronym: section["acronym"] as! String, code: section["code"] as! String))
                                        }
                                        
                                        //Indicate that the datas loading have work
                                        loaded = true
                                    }
                                }
                            }
                        }
                        
                        //Determinate if datas have been loaded
                        self.sectionsIsLoaded = loaded
                        self.sectionsIsLoading = false
                        
                        //free all threads which wait to the load
                        self.sectionsWaitOnLoad.set()
                    });
                }
            }
            
            //If we need to wait to the loading of sections
            if wait {
                sectionsWaitOnLoad.wait()
            }
        }
    }
    
    /**
     Load all beacon (all types) and books from the server
     
     - parameter wait:	This parameter indicate if we have to wait until the datas are load
     */
    open func loadBeaconsAndBooks(waitOnLoad wait: Bool) {
        //Verify if datas are not already loaded
        if !beaconsIsLoaded {
            //Verify if datas are not in loading
            if !beaconsIsLoading {
                //Set the flag of the Event to false for others attempt which want to load with wait
                beaconsWaitOnLoad.clear()
                
                let storage = Storage()
                var fromServer: Bool = true
                
                //If we need to load from local storage and data on the local storage are not expired
                if self.loadFromLocalStorage {
                    if let books = storage.getBooks(), !books.isEmpty {
                        self.books = books
                        if let iBeacons = storage.getiBeacons(), !iBeacons.isEmpty {
                            self.iBeacons = iBeacons
                            if let nearables = storage.getNearables() {
                                self.nearables = nearables
                                if let eddystones = storage.getEddystones() {
                                    //If the load successful affect data loaded otherwise load from internet
                                    self.eddystones = eddystones
                                    
                                    //If datas need to be updated try it
                                    if !storage.isNeedToUpdateData() {
                                        fromServer = false
                                        beaconsIsLoaded = true
                                        beaconsIsLoading = false
                                        
                                        //free all threads which wait to the load
                                        self.beaconsWaitOnLoad.set()
                                    }
                                }
                            }
                        }
                    }
                }
                
                if fromServer {
                    //Start loading in another thread
                    DispatchQueue.global(priority: BibAppAPI.DEFAULT_DATA_LOADING_PRIORITY).async(execute: { () in
                        
                        //Define if loadings have work
                        var iBeaconsLoaded: Bool = false
                        var nearablesLoaded: Bool = false
                        var eddystonesLoaded: Bool = false
                        var booksLoaded: Bool = false
                        
                        //Indicate that the data are in loading
                        self.beaconsIsLoading = true
                        
                        //Do the request
                        let httpRequestResult: HTTPRequestResult = HTTPRequest.submit(BibAppAPI.GET_BEACONS_AND_BOOKS, requestMethod: HTTPRequestMethod.GET, requestParameters: ["withContent": "true"])
                        
                        //Verifiy if the request have work and extract data
                        if httpRequestResult.responseCode == HTTPStatusCode.OK {
                            if let json = httpRequestResult.jsonObject as? NSDictionary {
                                if let data = json["data"] as? NSDictionary {
                                    
                                    if let ibeacons = data["iBeacons"] as? [[String: Any]] {
                                        
                                        self.iBeacons = []
                                        //Add each iBeacon found in the iBeacon data array
                                        for ibeacon in ibeacons {
                                            self.iBeacons.append(iBeacon(section: ibeacon["section"] as? Int, major: ibeacon["major"] as! Int, minor: ibeacon["minor"] as! Int))
                                        }
                                        
                                        //Indicate that the datas loading have work
                                        iBeaconsLoaded = true
                                    }
                                    
                                    if let nearables = data["nearables"] as? [[String: Any]] {
                                        
                                        self.nearables = []
                                        //Add each nearables found in the nearables data array
                                        for nearable in nearables {
                                            self.nearables.append(Nearables(section: nearable["section"] as? Int, id: nearable["id"] as! String))
                                        }
                                        
                                        //Indicate that the datas loading have work
                                        nearablesLoaded = true
                                    }
                                    
                                    if let eddystones = data["eddystones"] as? [[String: Any]] {
                                        
                                        self.eddystones = []
                                        //Add each eddystone found in the eddystone data array
                                        for eddystone in eddystones {
                                            self.eddystones.append(Eddystone(section: eddystone["section"] as? Int, instance: eddystone["instance"] as! String))
                                        }
                                        
                                        //Indicate that the datas loading have work
                                        eddystonesLoaded = true
                                    }
                                    
                                    if let books = data["books"] as? [[String: Any]] {
                                        
                                        self.books = []
                                        //Add each book found in the book data array
                                        for book in books {
                                            self.books.append(Book(id: book["id"] as! String, title: book["title"] as! String, author: book["author"] as! String, quote: book["quote"] as! String, image: book["image"] as! String, resume: book["resume"] as! String, language: book["language"] as! String, applicant: book["applicant"] as! String, year: book["year"] as! String, desc: book["description"] as! String, editor: book["editor"] as! String, section: book["section"] as! Int))
                                        }
                                        
                                        self.books.sort(by: {$0.section < $1.section})
                                        
                                        //Indicate that the datas loading have work
                                        booksLoaded = true
                                    }
                                }
                            }
                        }
                        
                        //Indicate if ALL datas have been loaded
                        self.beaconsIsLoaded = iBeaconsLoaded && nearablesLoaded && eddystonesLoaded && booksLoaded
                        self.beaconsIsLoading = false
                        
                        //free all threads which wait to the load
                        self.beaconsWaitOnLoad.set()
                    });
                }
            }
            
            //If we need to wait to the loading of datas
            if wait {
                beaconsWaitOnLoad.wait()
            }
        }
    }
    
    /**
     Start the loading of all data
     */
    open func initData() {
        loadSections(waitOnLoad: false)
        loadBeaconsAndBooks(waitOnLoad: false)
    }
    
    /**
     Store all datas on local storage for future load without the time of an internet request
     */
    open func storeDataOnLocalDisk() {
        loadSections(waitOnLoad: true)
        loadBeaconsAndBooks(waitOnLoad: true)
        
        let storage = Storage()
        storage.storeSections(self.sections)
        storage.storeBooks(self.books)
        storage.storeiBeacons(self.iBeacons)
        storage.storeNearables(self.nearables)
        storage.storeEddystones(self.eddystones)
        storage.dataUpdated()
    }
    
    /**
     Get all sections
     
     - returns: Array of sections
     */
    open func getSections() -> [Section] {
        loadSections(waitOnLoad: true)
        return self.sections
    }
    
    /**
     Get all books
     
     - returns: Array of book
     */
    open func getBooks() -> [Book] {
        loadBeaconsAndBooks(waitOnLoad: true)
        return self.books
    }
    
    /**
     Get all books from a section
     
     - parameter section:	Section object
     
     - returns: Array of books of the section
     */
    open func getBooks(forSection section: Section) -> [Book] {
        loadBeaconsAndBooks(waitOnLoad: true)
        return self.books.filter({$0.section == section.id})
    }
    
    /**
     Get a book for an specifiq id
     
     - parameter id:	ID of the book
     
     - returns: If a book have been found, the book object otherwise return nil
     */
    open func getBook(forID id: String) -> Book? {
        loadBeaconsAndBooks(waitOnLoad: true)
        return self.books.find({$0.id == id})
    }
    
    
    
    /**
     Get a section for an specifiq id
     
     - parameter id:	ID of the section
     
     - returns: If a section have been found, the section object otherwise return nil
     */
    open func getSection(forID id: Int) -> Section? {
        loadSections(waitOnLoad: true)
        return self.sections.find({$0.id == id})
    }
    
    /**
     Get an iBeacon data from his ids
     
     - parameter major:	major
     - parameter minor:	minor
     
     - returns: If an iBeacon have been found, the iBeacon object otherwise return nil
     */
    open func getIBeacon(_ major: Int, minor: Int) -> iBeacon? {
        loadBeaconsAndBooks(waitOnLoad: true)
        return self.iBeacons.find({$0.major == major && $0.minor == minor})
    }
    
    /**
     Get an nearables data from his id
     
     - parameter id:	id
     
     - returns: If an nearables have been found, the nearables object otherwise return nil
     */
    open func getNearables(_ id: String) -> Nearables? {
        loadBeaconsAndBooks(waitOnLoad: true)
        return self.nearables.find({$0.id == id})
    }
    
    /**
     Get an eddystone data from his id
     
     - parameter instance:	ID of the eddystone beacon
     
     - returns: If an eddystone have been found, the eddystone object otherwise return nil
     */
    open func getEddystone(_ instance: String) -> Eddystone? {
        loadBeaconsAndBooks(waitOnLoad: true)
        return self.eddystones.find({$0.instance == instance})
    }
    
    /**
     Get a complete (with setcion ID) beacon object from a another beacon object that don't have all informations
     
     - parameter beacon: The beacon with data to search
     
     - returns: The beacon found or nil if no beacon was found
     */
    open func getBeacon(_ beacon: Beacon) -> Beacon? {
        //Test the type and do the search of the beacon
        if let beacon = beacon as? iBeacon {
            return getIBeacon(beacon.major, minor: beacon.minor)
        } else if let beacon = beacon as? Nearables {
            return getNearables(beacon.id)
        } else if let beacon = beacon as? Eddystone {
            return getEddystone(beacon.instance)
        }
        
        return nil
    }
}
