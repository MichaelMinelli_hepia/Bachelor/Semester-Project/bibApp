from scrapy.spiders import Spider
from scrapy.selector import Selector

from dirbot.items import Website


class BookSpider(Spider):
    name = "book"
    allowed_domains = ["dmoz.org"]
    start_urls = [
        "http://recherche.nebis.ch/primo_library/libweb/action/search.do?fn=search&ct=search&initialSearch=true&mode=Basic&tab=default_tab&indx=1&dum=true&srt=rank&vid=NEBIS&frbg=&tb=t&vl%28freeText0%29=E41itel+AND+E41201510&scp.scps=scope%3A%28ebi01_prod%29&vl%28613971159UI1%29=all_items&vl%281UIStartWith0%29=contains&vl%28215043702UI0%29=any&vl%28215043702UI0%29=title&vl%28215043702UI0%29=any"
    ]

    def parse(self, response):
        """
        The lines below is a spider contract. For more info see:
        http://doc.scrapy.org/en/latest/topics/contracts.html

        @url http://www.dmoz.org/Computers/Programming/Languages/Python/Resources/
        @scrapes name
        """
        sel = Selector(response)
        sites = sel.xpath('//div[@class="EXLSummaryFields"]')
        items = []

        for site in sites:
            #Titre : print site.xpath('h2[@class="EXLResultTitle"]/a/text()').extract()
            #Lien : print site.xpath('h2[@class="EXLResultTitle"]/a/@href').extract()
            print site.xpath('h2[@class="EXLResultTitle"]/a/text()').extract()

            #print site.xpath('h3[@class="EXLResultAuthor"]/text()').extract()

            #Lien : print site.xpath('h2[@class="EXLResultTitle"]/a/@href').extract()
            #Lien : print site.xpath('h2[@class="EXLResultTitle"]/a/@href').extract()
            #Lien : print site.xpath('h2[@class="EXLResultTitle"]/a/@href').extract()

            #item = Website()
            #item['name'] = site.xpath('a/text()').extract()
            #item['url'] = site.xpath('a/@href').extract()
            #item['description'] = site.xpath('text()').re('-\s[^\n]*\\r')
            #items.append(item)

        return items
