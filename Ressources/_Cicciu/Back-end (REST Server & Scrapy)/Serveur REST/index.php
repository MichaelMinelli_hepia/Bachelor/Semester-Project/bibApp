<?php
	/**********************************************
		Rest service for extract data for bibApp
		
		Authors : Michaël Minelli, Salvatore Cicciu
		PHP version : >= 7.0 (!important)
	**********************************************/	
	
	require_once __DIR__ . '/include/constantes.inc.php';
	require_once __DIR__ . '/include/function.inc.php';
	
	require_once __DIR__ . '/SQL/connexion.sql.php';
	require_once __DIR__ . '/SQL/section.sql.php';
	require_once __DIR__ . '/SQL/book.sql.php';
	require_once __DIR__ . '/SQL/iBeacon.sql.php';
	require_once __DIR__ . '/SQL/Nearables.sql.php';
	require_once __DIR__ . '/SQL/Eddystone.sql.php';
	
	SectionSQL::prepare();
	BookSQL::prepare();
	iBeaconSQL::prepare();
	NearablesSQL::prepare();
	EddystoneSQL::prepare();
	
	//Init table of datas
	$result = array();
	
	$result["statusMessage"] = "";
	$result["timestamp"] = date(DATE_ATOM);
	$result["data"] = array();
	$data = &$result["data"]; 

	//End the script
	//Parameters : - Export type (json, xml)
	//			   - optional : Error message
	function endScript(string $typeExport, string $message = "") {
		global $result;
		
		//Test if there is an error
		if ($message != "") {
			http_response_code(400); //error code
			$result["statusMessage"] = $message;
			$result["data"] = array();
		} else 
			unset($result["statusMessage"]); //Remove error message row
	
		//Export in choosed format
		if ($typeExport == "json") {
			header('Content-Type: application/json; charset=utf8');
			echo json_encode($result, JSON_UNESCAPED_UNICODE);
		} else if ($typeExport == "xml") {
			header('Content-Type: application/xml; charset=utf8');
			echo xml_encode($result); //Own function (include/function.inc.php)
		} else { 
			//If the format isn't supported return an error
			http_response_code(501); 
			
			header('Content-Type: application/json; charset=utf8');
			$result["statusMessage"] = "Type d'export non disponible.";
			$result["data"] = array();
			echo json_encode($result, JSON_UNESCAPED_UNICODE);
		}
		
		//Stop the script execution
		die();
	}
	
	function sectionsTreatment($sections) {
		$result = array();
		
		foreach ($sections as $section) {
			$section["id"] = (int) $section["id"];
			array_push($result, $section);
		}
		
		return $result;
	}
	
	function booksTreatment($books) {
		$result = array();
		
		foreach ($books as $book) {
			$book["section"] = (int) $book["section"];
			$book["author"] = $book["author"] ?? ""; 
			$book["quote"] = $book["quote"] ?? ""; 
			$book["image"] = $book["image"] ?? ""; 
			$book["resume"] = $book["resume"] ?? ""; 
			$book["applicant"] = $book["applicant"] ?? ""; 
			$book["description"] = $book["description"] ?? ""; 
			$book["editor"] = $book["editor"] ?? ""; 
			array_push($result, $book);
		}
		
		return $result;
	}
	
	function beaconsTreatment($beacons) {
		$result = array();
		
		foreach ($beacons as $beacon) {
			$beacon["section"] = (int) $beacon["section"];
			array_push($result, $beacon);
		}
		
		return $result;
	}
	
	function iBeaconsTreatment($beacons) {
		$result = array();
		
		foreach ($beacons as $beacon) {
			$beacon["major"] = (int) $beacon["major"];
			$beacon["minor"] = (int) $beacon["minor"];
			$beacon["section"] = (int) $beacon["section"];
			array_push($result, $beacon);
		}
		
		return $result;
	}
	
	//Verify if the page parameter exists
	if (isset($_REQUEST['page'])) {
		$pageNameInfos = explode(".", $_REQUEST['page']);
		
		//Extract page name
		$page = $pageNameInfos[0];
		
		//Extract the format of export wwww.xyz/page.format?param1=z, default format is json
		$typeExport = $pageNameInfos[1] ?? "json";
		
		//Test what datas we have to give
		if ($page == "section")
			$data[SECTION] = sectionsTreatment(SectionSQL::getAll()); //Extract all sections
		else if ($page == "book") {
			$pointer = &$data[BOOK];
			$pointer = array();
			
			//Test if the user want to extract books of a specific beacon
			if (isset($_REQUEST['type'])) {
				//Test the beacon type (iBeacon, Nearables, Eddystone)
				switch($_REQUEST['type']){
				    case 'iBeacon':
				    	//Verify if all needed parameters are given
				    	if (!(isset($_REQUEST['major']) && isset($_REQUEST['minor'])))
				    		endScript($typeExport, "Des paramètres sont manquants.");
				    		
				    	//Extract data from the database
				    	$pointer = booksTreatment(iBeaconSQL::getBooksForBeacon($_REQUEST['major'], $_REQUEST['minor']));
				    	break;
				    case 'Nearables':
				    	//Verify if all needed parameters are given
				    	if (!isset($_REQUEST['id']))
				    		endScript($typeExport, "Des paramètres sont manquants.");
				    		
				    	//Extract data from the database
				    	$pointer = booksTreatment(NearablesSQL::getBooksForBeacon($_REQUEST['id']));
				    	break;
				    case 'Eddystone':
				    	//Verify if all needed parameters are given
				    	if (!isset($_REQUEST['instance']))
				    		endScript($typeExport, "Des paramètres sont manquants.");
				    		
				    	//Extract data from the database
				    	$pointer = booksTreatment(EddystoneSQL::getBooksForBeacon($_REQUEST['instance']));
						break;
				    default:
				    	//Error if the type of beacon is not supported
				    	endScript($typeExport, "Le type de beacon n'est pas reconnu.");
				}
			} else
				//Extract all books from the database
				$pointer = booksTreatment(BookSQL::getAll());
		} else if (in_array($page, array("iBeacon", "nearables", "eddystone", "allBeacons"))) {
			//Test if the user want to have data of one type or all types of beacon
			
			//withContent paramter is for if the user want to have book in relation with type of beacon he want to have
			$withContent = isset($_REQUEST['withContent']) ? $_REQUEST['withContent'] == 'true' : false;
			
			//Test if the user want to have the data of all type of beacon
			$allBeacons = $page == "allBeacons";
		
			//Test if user want a specific type of beacon or all and extract data of beacon from the database
			if ($page == "iBeacon" || $allBeacons)
				$data[IBEACON] = iBeaconsTreatment(iBeaconSQL::getAll());
			
			if ($page == "nearables" || $allBeacons)
				$data[NEARABLES] = beaconsTreatment(NearablesSQL::getAll());
			
			if ($page == "eddystone" || $allBeacons)				
				$data[EDDYSTONE] = beaconsTreatment(EddystoneSQL::getAll());
			
			//If the user want books which are used by beacons, test the type he want and extract books.
			if ($withContent) {
				if ($allBeacons)
					$data[BOOK] = booksTreatment(BookSQL::getAllUsed());
				else if ($page == "iBeacon")
					$data[BOOK] = booksTreatment(iBeaconSQL::getBooks());
				else if ($page == "nearables")
					$data[BOOK] = booksTreatment(NearablesSQL::getBooks());
				else if ($page == "eddystone")
					$data[BOOK] = booksTreatment(EddystoneSQL::getBooks());
			}
		} else
			endScript($typeExport, "La page demandée est inconnue.");
		
		endScript($typeExport);
	}
	
	//Error no page parameter found
	endScript("json", "Aucune pages spécifiée.");
?>
