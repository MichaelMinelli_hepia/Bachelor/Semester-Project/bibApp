#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from warnings import filterwarnings
import MySQLdb
import sys
import os
import time

HEPIA_ID = "E41"

MySQL_ADDRESS  = "bibapp.infolibre.ch"
MySQL_DATABASE = "bibApp"
MySQL_USERNAME = "bibapp_scrapcraw"
MySQL_PASSWORD = "WJs46Nd46sLJ9NTM"
MySQL_PORT     = 9906

filterwarnings('ignore', category = MySQLdb.Warning)

print "********************* " + time.strftime("%d/%m/%Y %H:%M:%S") + " **********************"

try:
    con = MySQLdb.connect(host=MySQL_ADDRESS, user=MySQL_USERNAME, passwd=MySQL_PASSWORD, db=MySQL_DATABASE, port=MySQL_PORT)
except MySQLdb.Error, e:
    print "Erreur lors de la connexion à la base de données.\n Error %d: %s" % (e.args[0],e.args[1])
    exit(-1)

cur = con.cursor(MySQLdb.cursors.DictCursor)

# For every section execute the scrapy script
try:
    cur.execute("SELECT * FROM Section")

    rows = cur.fetchall()
    os.system("echo $PATH");
    for row in rows:
        print "Passage aux ouvrages de la section : " + row['section_name']
        os.system("scrapy crawl book -a section=%s -a sectionID=%s" % (row['section_code'], row['section_id']))
except:
    print "Erreur : La liste des section n'a pas peu être faite."
    exit(-1)

# Delete book for more of three month
now = time.localtime()
firstSecondOfLastThreeMonth = time.mktime((now.tm_year, now.tm_mon - 3, 1, 0, 0, 0, 0, 0, 0))

try:
    cur.execute("DELETE FROM Book WHERE book_insert_date < %s", (firstSecondOfLastThreeMonth, ))
except MySQLdb.Warning, e:
    pass
except:
    pass

con.commit()

con.close()
