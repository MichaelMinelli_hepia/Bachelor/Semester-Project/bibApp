//
//  ViewController.swift
//  Decouverte iOS iBeacon
//
//  Created by Michaël Minelli on 22.10.15.
//  Copyright © 2015 Michaël Minelli. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController, iBeaconDetectionDelegate {

    @IBOutlet weak var txtShow: UITextView!
    @IBOutlet var view: UIView!
    
    var ibeaconDetection: iBeaconDetection?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        ibeaconDetection = iBeaconDetection(viewController: self, actualMinor: 0, delegate: self)
        ibeaconDetection!.run()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(animated: Bool) {
        //locationManager.stopRangingBeaconsInRegion(self.beaconRegion)
    }
    
    func onMinorChange(newMinor: Int) {
        txtShow.text = txtShow.text + "\n" + String(newMinor)
    }
    
}