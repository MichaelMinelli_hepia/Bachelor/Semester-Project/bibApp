<?php

class BookSQL extends SQLConnect
{
    private static $getAll, $getAllUsed;

    public static function prepare() {
        self::$getAll = self::$db->prepare(
            "SELECT book_id as 'id', book_title as 'title', book_authors as 'author', book_quote as 'quote', book_image as 'image', book_resume as 'resume', book_language as 'language', book_applicant as 'applicant', book_year as 'year', book_description as 'description', book_editor as 'editor', book_section as 'section' FROM Book ORDER BY book_insert_date DESC;"
        );
        
        self::$getAllUsed = self::$db->prepare(
            "SELECT book_id as 'id', book_title as 'title', book_authors as 'author', book_quote as 'quote', book_image as 'image', book_resume as 'resume', book_language as 'language', book_applicant as 'applicant', book_year as 'year', book_description as 'description', book_editor as 'editor', book_section as 'section' FROM Book WHERE book_section IN (SELECT DISTINCT(ibeacon_section) FROM iBeacon WHERE ibeacon_status = 1) OR book_section IN (SELECT DISTINCT(nearables_section) FROM Nearables WHERE nearables_status = 1) OR book_section IN (SELECT DISTINCT(eddystone_section) FROM Eddystone WHERE eddystone_status = 1) ORDER BY book_insert_date DESC;"
        );
    }

    public static function getAll() {
        self::$getAll->execute();

        return self::$getAll->fetchAll();
    }
    
    public static function getAllUsed() {
        self::$getAllUsed->execute();

        return self::$getAllUsed->fetchAll();
    }
}
?>