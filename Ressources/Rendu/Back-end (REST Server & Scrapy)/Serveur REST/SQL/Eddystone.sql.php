<?php

class EddystoneSQL extends SQLConnect
{
    private static $getAll, $getBooksForBeacon, $getBooks;

    public static function prepare() {
        self::$getAll = self::$db->prepare(
            "SELECT eddystone_instance as 'instance', eddystone_section as 'section' FROM Eddystone WHERE eddystone_status = 1 ORDER BY eddystone_instance;"
        );
        
        self::$getBooksForBeacon = self::$db->prepare(
	        "SELECT book_id as 'id', book_title as 'title', book_authors as 'author', book_quote as 'quote', book_image as 'image', book_resume as 'resume', book_language as 'language', book_applicant as 'applicant', book_year as 'year', book_description as 'description', book_editor as 'editor', book_section as 'section' FROM Book JOIN Eddystone ON book_section = eddystone_section WHERE eddystone_instance=:instance ORDER BY book_insert_date DESC;"
        );
        
        self::$getBooks = self::$db->prepare(
            "SELECT book_id as 'id', book_title as 'title', book_authors as 'author', book_quote as 'quote', book_image as 'image', book_resume as 'resume', book_language as 'language', book_applicant as 'applicant', book_year as 'year', book_description as 'description', book_editor as 'editor', book_section as 'section' FROM Book WHERE book_section IN (SELECT DISTINCT(eddystone_section) FROM Eddystone WHERE eddystone_status = 1) ORDER BY book_insert_date DESC;"
        );
    }

    public static function getAll() {
        self::$getAll->execute();

        return self::$getAll->fetchAll();
    }
    
    public static function getBooksForBeacon($instance) {
        self::$getBooksForBeacon->execute(array(
	        ':instance' => $instance
        ));

        return self::$getBooksForBeacon->fetchAll();
    }
    
    public static function getBooks() {
        self::$getBooks->execute();

        return self::$getBooks->fetchAll();
    }
}
?>