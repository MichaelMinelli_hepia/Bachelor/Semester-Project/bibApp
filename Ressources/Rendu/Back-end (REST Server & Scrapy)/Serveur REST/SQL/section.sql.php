<?php

class SectionSQL extends SQLConnect
{
    private static $getAll;

    public static function prepare() {
        self::$getAll = self::$db->prepare(
            "SELECT section_id as 'id', section_name as 'name', section_acronym as 'acronym', section_code as 'code' FROM Section ORDER BY section_name;"
        );
    }

    public static function getAll() {
        self::$getAll->execute();

        return self::$getAll->fetchAll();
    }
}
?>