-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Client :  bibapp.infolibre.ch:9906
-- Généré le :  Dim 28 Février 2016 à 00:56
-- Version du serveur :  5.5.47-0+deb8u1
-- Version de PHP :  5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bibApp`
--
CREATE DATABASE IF NOT EXISTS `bibApp` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `bibApp`;

-- --------------------------------------------------------

--
-- Structure de la table `Book`
--

CREATE TABLE IF NOT EXISTS `Book` (
  `book_id` varchar(20) COLLATE utf8_bin NOT NULL,
  `book_title` varchar(512) COLLATE utf8_bin NOT NULL,
  `book_authors` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  `book_quote` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `book_image` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `book_resume` longtext COLLATE utf8_bin,
  `book_language` varchar(45) COLLATE utf8_bin NOT NULL,
  `book_applicant` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `book_year` year(4) NOT NULL,
  `book_description` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  `book_editor` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `book_insert_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `book_section` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `Eddystone`
--

CREATE TABLE IF NOT EXISTS `Eddystone` (
  `eddystone_instance` varchar(6) COLLATE utf8_bin NOT NULL,
  `eddystone_datetime` datetime NOT NULL,
  `eddystone_section` int(11) NOT NULL,
  `eddystone_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `Nearables`
--

CREATE TABLE IF NOT EXISTS `Nearables` (
  `nearables_id` varchar(8) COLLATE utf8_bin NOT NULL,
  `nearables_datetime` datetime NOT NULL,
  `nearables_section` int(11) NOT NULL,
  `nearables_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `Periodic`
--

CREATE TABLE IF NOT EXISTS `Periodic` (
  `periodic_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `periodic_image` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `periodic_lastTitle` varchar(128) COLLATE utf8_bin NOT NULL,
  `periodic_date` datetime NOT NULL,
  `periodic_section` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `Section`
--

CREATE TABLE IF NOT EXISTS `Section` (
  `section_id` int(11) NOT NULL,
  `section_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `section_acronym` varchar(5) COLLATE utf8_bin NOT NULL,
  `section_code` varchar(10) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `iBeacon`
--

CREATE TABLE IF NOT EXISTS `iBeacon` (
  `ibeacon_major` int(16) NOT NULL,
  `ibeacon_minor` int(16) NOT NULL,
  `ibeacon_datetime` datetime NOT NULL,
  `ibeacon_section` int(11) NOT NULL,
  `ibeacon_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `Book`
--
ALTER TABLE `Book`
  ADD PRIMARY KEY (`book_id`),
  ADD KEY `book_secction_fk_idx` (`book_section`);

--
-- Index pour la table `Eddystone`
--
ALTER TABLE `Eddystone`
  ADD PRIMARY KEY (`eddystone_instance`),
  ADD KEY `eddystone_section_fk_idx` (`eddystone_section`);

--
-- Index pour la table `Nearables`
--
ALTER TABLE `Nearables`
  ADD PRIMARY KEY (`nearables_id`),
  ADD KEY `nearables_section_fk_idx` (`nearables_section`);

--
-- Index pour la table `Periodic`
--
ALTER TABLE `Periodic`
  ADD PRIMARY KEY (`periodic_name`),
  ADD KEY `periodic_section_pk_idx` (`periodic_section`);

--
-- Index pour la table `Section`
--
ALTER TABLE `Section`
  ADD PRIMARY KEY (`section_id`);

--
-- Index pour la table `iBeacon`
--
ALTER TABLE `iBeacon`
  ADD PRIMARY KEY (`ibeacon_major`,`ibeacon_minor`),
  ADD KEY `ibeacon_section_fk_idx` (`ibeacon_section`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `Section`
--
ALTER TABLE `Section`
  MODIFY `section_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `Book`
--
ALTER TABLE `Book`
  ADD CONSTRAINT `book_secction_fk` FOREIGN KEY (`book_section`) REFERENCES `Section` (`section_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `Eddystone`
--
ALTER TABLE `Eddystone`
  ADD CONSTRAINT `eddystone_section_fk` FOREIGN KEY (`eddystone_section`) REFERENCES `Section` (`section_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `Nearables`
--
ALTER TABLE `Nearables`
  ADD CONSTRAINT `nearables_section_fk` FOREIGN KEY (`nearables_section`) REFERENCES `Section` (`section_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `Periodic`
--
ALTER TABLE `Periodic`
  ADD CONSTRAINT `periodic_section_fk` FOREIGN KEY (`periodic_section`) REFERENCES `Section` (`section_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `iBeacon`
--
ALTER TABLE `iBeacon`
  ADD CONSTRAINT `ibeacon_section_fk` FOREIGN KEY (`ibeacon_section`) REFERENCES `Section` (`section_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
