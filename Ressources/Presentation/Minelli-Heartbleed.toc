\beamer@endinputifotherversion {3.36pt}
\select@language {french}
\beamer@sectionintoc {1}{Sujet}{4}{0}{1}
\beamer@sectionintoc {2}{Beacons}{5}{0}{2}
\beamer@subsectionintoc {2}{1}{G\'en\'eralit\'es}{5}{0}{2}
\beamer@subsectionintoc {2}{2}{iBeacon}{6}{0}{2}
\beamer@subsectionintoc {2}{3}{Utilisation sous iOS}{7}{0}{2}
\beamer@sectionintoc {3}{Architecture}{8}{0}{3}
\beamer@sectionintoc {4}{Back-end}{9}{0}{4}
\beamer@sectionintoc {5}{Front-end}{10}{0}{5}
\beamer@subsectionintoc {5}{1}{Structure du programme}{10}{0}{5}
\beamer@subsectionintoc {5}{2}{Interface \& D\'emonstration}{11}{0}{5}
\beamer@sectionintoc {6}{Conclusion}{12}{0}{6}
\beamer@sectionintoc {7}{Questions}{13}{0}{7}
